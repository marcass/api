# api

This project is aimed at rapid redeplyoment of microservices when a machine falls over with a view to hosting eslewhere if necessary

## Structure

* Kong api gateway
* Python3 flask api's
* ingress from anything that can make a RESTful request via https
* reporting via web interface via gateways/api's

## Deployment

### Kong

Deploy images via docker compose and have a persistent kong data store (back this up)
* Seperate network to data store for sensors
* allow kong to access network for api's and datastore, as well as kong-net for it's own stuff
* setup instuctions in ./docker/kong/kong_setup via RESTful admin api on localhost


### Sensor datastore

* Using influx db
* Influx and pyhton3 gunicorn served api's on same network
* mounted volume for backups of datastore with influxdb

### Webserver

* Nginx (local installation as certbot is a pain in the ass with docker)
* certbot for cert

